# AZLA Learn Azerbaijani

Azerbaijani vocabulary words.

![Flag](./images/flag.jpg)

## TABLE OF CONTENTS

- [ABOUT](#about)
- [DEPENDENCIES](#dependencies)
  - [ARCH](#arch)
- [INSTALL](#install)
- [SUFFIX](#suffix)
- [EXAMPLE WORDS](#example-words)
  - [BASICS](#basics)
  - [EXTRAS](#extras)
  - [GREETINGS](#greetings)
  - [MONTHS](#months)
  - [WEEKDAYS](#weekdays)
  - [FAMILY MEMBERS](#family-members)
  - [VERBS IN INFINITE](#verbs-in-infinite)
- [LICENSE](#license)

## ABOUT

**ABOUT**

Azla is a GUI application that I wrote using Lua to help me remember Azerbaijani vocabulary words. It is not feature complete yet, and there are many ways I can make it better, but right now it is usable. All my wordlists are located in `lua_words`.

## DEPENDENCIES

**DEPENDENCIES**

Libraries needed to run this app are `lgi`, `lfs`, `G0Object`, and `GdkPixbuf`. If you clone this repo and want to run this app, you need to have those libraries installed.

### ARCH

On Arch Linux, you can run these commands to install all dependencies to build from source:

```bash
sudo paru -S luarocks gtk4 lua-lgi-git lua53-lgi luastatic lua53 lua

# Install some Lua modules
sudo luarocks install filesystem luautf8 dkjson
```

## INSTALL

**INSTALL**

All you need to do to install this is to run `install.sh`. Remember that you need to install all dependencies manually. This script will move all files we need to `/opt/azla` and build the binary from source.

```bash
git clone https://github.com/phoenix988/azla.git
cd azla
./install.sh
```

## SUFFIX

**SUFFIX**

[About the rules of suffix in Azerbaijani language](https://github.com/phoenix988/azla/tree/dev/suffix)

## EXAMPLE WORDS

**EXAMPLE WORDS**

Some important Azerbaijani vocabulary words to know that will help me learn the Azerbaijani language. There is also a script that works like an exam and will give you a sentence in English or Azerbaijani, and you need to translate it.

### Nouns

This is some nouns in Azerbaijani:

| Azerbaijani | English  |
| ----------- | -------- |
| Kino        | Movie    |
| Dəftər      | Notebook |
| Kilsə       | Church   |

### Adjectives

This is some adjectives in Azerbaijani:

| Azerbaijani | English    |
| ----------- | ---------- |
| Tez         | Early/Fast |
| Gec         | Late       |
| Təzliklə    | Soon       |

### Phrases

This is some common phrases in Azerbaijani:

| Azerbaijani        | English         |
| ------------------ | --------------- |
| Ügurlar            | Good Luck       |
| Xoş gördük         | Good to see you |
| Kimi               | Who             |
| Ola biler          | Maybe           |
| Var                | There is        |
| Hara               | Where           |
| Hansı              | Which           |
| Sənin nə yaşın var | How old are you |

### MONTHS

Months in Azerbaijani:

| Azerbaijani                 | English                |
| --------------------------- | ---------------------- |
| Ay                          | Month                  |
| Yanvar                      | January                |
| Fevral                      | February               |
| Mart                        | March                  |
| Aprel                       | April                  |
| May                         | May                    |
| Iyun                        | June                   |
| İyul                        | July                   |
| Avqust                      | August                 |
| Sentyabr                    | September              |
| Oktyabr                     | October                |
| Noyabr                      | November               |
| Dekabr                      | December               |
| Tarix                       | Date                   |
| Bir sentyabr                | September 1            |
| Fevralın on beşi            | February 15            |
| Yanvarın iyirmi üçü         | January 23             |
| first                       | birinci                |
| Sizin ad gününüz nə vaxtdır | When is your birthday  |
| Bu gün ayın neçəsidir       | What is the date today |

### FAMILY MEMBERS

Family words in Azerbaijani:

| Azerbaijani | English       |
| ----------- | ------------- |
| Bacı        | Sister        |
| Qardaş      | Brother       |
| Uşaq        | Child         |
| Nənə        | Grandmother   |
| Baba        | Grandfather   |
| Bibi        | Aunt          |
| Əmi         | Uncle         |
| Xalaqızı    | Cousin        |
| Qayınata    | Father-in-law |
| Qayınana    | Mother-in-law |
| Sevgili     | Girlfriend    |
| Ailə        | Family        |
| Nişanlı qız | Fiance        |
| Nişanlı     | Engaged       |

### VERBS IN INFINITE

Verbs in infinite form in Azerbaijani:

| Azerbaijani | English       |
| ----------- | ------------- |
| Bağlamaq    | to close      |
| Başa duşmək | to understand |
| Fikirləşmək | to think      |
| Qurtamaq    | to end        |
| Qulaq asmaq | to listen     |
| Yazmaq      | to write      |
| Baxmaq      | to look       |
| Götürmək    | to take       |
| Bilmək      | to know       |
| Getmək      | to go         |
| Gəlmək      | to come       |
| Işləmək     | to work       |
| Öyrənmək    | to learn      |
| Oxumaq      | to read       |
| Yemək       | to eat        |
| İçmək       | to drink      |
| Açmaq       | to open       |
| Vermək      | to give       |
| Görmək      | to see        |
| Oynamaq     | to play       |
| Oyanmaq     | to wake up    |
| Sincəlmeq   | to rest       |
| Almaq       | to buy        |
| Yumaq       | to wash       |
| Olmaq       | to be         |
| Alğamaq     | to cry        |

## LICENSE

[MIT License](https://choosealicense.com/licenses/mit/)

```

```
